﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.Text.Json;
using csv= ServiceStack.Text;

namespace OtusTaskOne
{
    
    interface ISerializer<T>
    {
        string Serialize(T[] item);
        T[] Deserialize(Stream stream);
    }

    class OtusCsvSerializer<T> : ISerializer<T>
    {
        public T[] Deserialize(Stream stream)
        {
            return csv.CsvSerializer.DeserializeFromStream<T[]>(stream);
        }

        public string Serialize(T[] array)
        {
            return csv.CsvSerializer.SerializeToCsv(array);
        }
    }

    class OtusJsonSerializer<T> : ISerializer<T>
    {
        public T[] Deserialize(Stream stream)
        {
            using(StreamReader reader = new StreamReader(stream))
            {
                return JsonSerializer.Deserialize<T[]>(reader.ReadToEnd());
            }

        }

        public string Serialize(T[] array)
        {
            return JsonSerializer.Serialize(array);
        }
    }

    class OtusXmlSerializer<T> : ISerializer<T>
    {
        private IExtendedXmlSerializer serializer;
        private XmlWriterSettings wSettings; 
        private XmlReaderSettings rSettings; 

        public OtusXmlSerializer()
        {
            serializer = new ConfigurationContainer().UseAutoFormatting()
                .UseOptimizedNamespaces()
                .EnableImplicitTyping(typeof(Person)).Create();

            wSettings = new XmlWriterSettings { Indent = true };
            rSettings = new XmlReaderSettings { IgnoreWhitespace = false };
        }
        public T[] Deserialize(Stream stream)
        {
            return serializer.Deserialize<T[]>(rSettings, stream);
        }

        public string Serialize(T[] item)
        {
            return serializer.Serialize(wSettings, item);
        }
    }
}
