﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using OtusTaskOne;
using System.IO;
using System.Linq;

namespace OtusTaskOne.Task3
{
    class Repository : IRepository<Account>
    {
        OtusJsonSerializer<Account> jsonSerializer;
        static string path = @"G:\Books\Repo.json";
        public Repository()
        {
            jsonSerializer = new OtusJsonSerializer<Account>();
        }
        
        public void Add(Account items)
        {
            var date = jsonSerializer.Serialize(new Account[] { items });
            File.AppendAllText(path, date);
        }

        public IEnumerable<Account> GetAll()
        {
            foreach(var k in jsonSerializer.Deserialize(new StreamReader(path).BaseStream))
            {
                yield return k;
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            return GetAll().FirstOrDefault(predicate);
        }
    }

    interface IRepository<T>
    {
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
    }
}
