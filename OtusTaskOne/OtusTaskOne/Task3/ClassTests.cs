﻿using ExtendedXmlSerializer;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace OtusTaskOne.Task3
{
    class ClassTests
    {
        [Fact]
        public void AddAccountTest()
        {
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(x => x.GetAll()).Returns(Get_All());
            mock.Setup(x => x.Add(It.IsAny<Account>()));
            //mock.Setup(x=> x.GetOne(It.Is<Account>(v => v==true )))).Returns(books[0]);

            IAccountService accountService = new AccountService(mock.Object);
            accountService.AddAccount(new Account());

            mock.Verify(m => m.Add(new Account()));


            
        }


        IEnumerable<Account> Get_All()
        {
            return new Account[]
            {
                new Account {FirstName="Gleb", LastName="Laskov", BirthDate=DateTime.Now},
                new Account {FirstName="Ylan", LastName="Russian", BirthDate = DateTime.Now}
            };
        }
    }

   
}
