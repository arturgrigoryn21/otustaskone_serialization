﻿using OtusTaskOne.Task3;
using System;
using System.Collections.Generic;
using System.Text;

namespace OtusTaskOne
{
    interface IAccountService
    {
        void AddAccount(Account account);
    }

    class AccountService : IAccountService
    {
        IRepository<Account> repository;

        public AccountService(IRepository<Account> repository)
        {
            this.repository = repository;
        }
        public void AddAccount(Account account)
        {
            account = new Account() { FirstName = "Gleb", LastName = "laskov", BirthDate = DateTime.Now };
            repository.Add(account);
        }
    }

    class Account
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }

    
}
