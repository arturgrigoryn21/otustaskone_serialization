﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace OtusTaskOne
{
    interface IAlgorithm<T>
    {
        public IEnumerable<T> Sort(IEnumerable<T> innerCol);
    }

    class Algorithm : IAlgorithm<Account>
    {
        public IEnumerable<Account> Sort(IEnumerable<Account> innerCol)
        {
            return innerCol.OrderBy(x => x.LastName);
        }
    }
}
