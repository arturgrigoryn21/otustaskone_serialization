﻿using System;
using System.IO;
using OtusTaskOne.Task3;

namespace OtusTaskOne
{
    class Program
    {
        static string path = @"G:\Books\Test.xml";
        static string path2 = @"G:\Books\Test.json";
        static string path3 = @"G:\Books\Test.csv";
        static OtusXmlSerializer<Person> xSerializer;
        static OtusJsonSerializer<Person> jsonSerializer;
        static OtusCsvSerializer<Person> csvSerializer;
        static Person[] persons;
        static void Main(string[] args)
        {
            Init();
            
            Repository repository = new Repository();
            //repository.Add(new Account[]
            //{
            //    new Account {FirstName="Chip", LastName="Genrios", BirthDate=DateTime.Now},
            //    new Account {FirstName="Bitch", LastName="Chermander", BirthDate=DateTime.Now},
            //    new Account {FirstName="Kvantic", LastName="Mister", BirthDate = DateTime.Now}
            //});
            //foreach(var k in repository.GetAll())
            //    Console.WriteLine(k.FirstName);

            Console.WriteLine(repository.GetOne(x=>x.FirstName== "Kvantic").LastName);

            //Account account = repository.GetOne(x => x.LastName == "Art");


            

            //DeSerialize(path3, csvSerializer);
        }



        static void DeSerialize(ISerializer<Person> serializer, string path)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                foreach(var k in new OtusStreamReader<Person>(reader.BaseStream, serializer))
                {
                    Console.WriteLine(k.SecondName, k.Name);
                }
            }
        }

        static void DeSerialize(string path, ISerializer<Person> serializer)
        {
            using (StreamReader reader = new StreamReader(path))
            {
                var stream = reader.BaseStream;
                var pers = serializer.Deserialize(stream);
                for (int l = 0; l < pers.Length; l++)
                    Console.WriteLine(pers[l].SecondName, pers[l].Name);
            }
        }
        static void Serialize(string path, ISerializer<Person> serializer)
        {
            var document = serializer.Serialize(persons);
            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.Write(document);
            }

            if (File.Exists(path))
            {
                Console.WriteLine("True");
            }
            else
                Console.WriteLine("False");
        }

        static void Init()
        {
            persons = new Person[]
            {
                new Person() {Name="Artur", SecondName="Grig" },
                new Person() {Name="Dag", SecondName="Stan" },
                new Person() {Name="Viktor", SecondName="Salagubov" }
            };

            xSerializer = new OtusXmlSerializer<Person>();
            jsonSerializer = new OtusJsonSerializer<Person>();
            csvSerializer = new OtusCsvSerializer<Person>();
        }
    }
}

