﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.Linq;

namespace OtusTaskOne
{
    class Person 
    {
        public string Name { get; set; }
        public string SecondName { get; set; }
    }
}
