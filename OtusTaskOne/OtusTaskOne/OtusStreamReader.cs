﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace OtusTaskOne
{
    class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private Stream stream;
        private ISerializer<T> serializer;

        public OtusStreamReader(Stream stream, ISerializer<T> serializer)
        {
            this.stream = stream;
            this.serializer = serializer;
        }

        public void Dispose()
        {
            ((IDisposable)stream).Dispose();
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var k in serializer.Deserialize(stream))
            {
                yield return k;
            }
        }


        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
